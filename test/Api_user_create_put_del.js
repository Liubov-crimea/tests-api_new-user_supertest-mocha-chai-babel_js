import supertest from 'supertest';
import { expect } from 'chai';

const request = supertest('https://gorest.co.in/public-api/');

const TOKEN = '63d41dd0f7017893414274c85256a7487572ce4f0c2e9499477ea551fbf3412b';

describe('New User', () => {
   
    let user_id;

    it ('Create new user', () => {
        const data = {
            email: `test-${Math.floor(Math.random() * 9999)}@mail.ua`,  //поля по документации
            name: 'Vasa',
            gender: 'male',
            status: 'inactive'   
        };

        return request
        .post('users')
        .set("Authorization", `Bearer ${TOKEN}`) 
        .send(data)
        .then((res)=>{
            console.log(res.body);
            //expect (res.body.data).to.deep.include(data);
            expect (res.body.data.email).to.eq(data.email)
            expect (res.body.data.status).to.eq(data.status)
            user_id = res.body.data.id;
            console.log("New user's id is "+user_id);
        })
    });

    it('Update new users name&status', async () => {   

        const res = await request

        const data = {
            status: 'active',
            name: `Pavel - ${Math.floor(Math.random() * 9999)}`         
        };

        return request
        .put(`users/${user_id}`)
        .set("Authorization", `Bearer ${TOKEN}`) 
        .send(data)
        .then((res)=>{
            console.log("There is info about user with updated name&status "+res.body.data);
            expect(res.body.data).to.deep.include(data) 
               
        })
    });

    it ('Delete our new user', async () => {   
        
        const res = await request

        return request
        .delete(`users/${user_id}`)
        .set("Authorization", `Bearer ${TOKEN}`) 
        .then((res)=>{
            //console.log(res.body.data);
            expect(res.body.data).to.be.eq(null) 
               
        })
    });

    it ('Chack new user is absent', async () => {

        const res = await request

        request
           .get(`users/${user_id}?access-token=${TOKEN}`) 
           .end((err,res)=>{
            
             console.log(res.body);  
             expect(res.body).to.be.empty;
             expect(res.body.message).to.be.eq("Resource not found") 
           });
    }); 

    

})